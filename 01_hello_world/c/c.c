#include "common.h"

extern void do_stuff();

int main(void){
   SET_CFG(rcc_apb2pcenr,1,1,5);

   SET_CFG(pd_cfg_low,CNF_GENERAL_OPEN_DRAIN,2,14);
   SET_CFG(pd_cfg_low,PINMODE_OUTPUT_SLOW,2,12);
   SET_CFG(pd_output,0,16,0);
   SET_CFG(pd_set_reset,0,32,0);
   SET_CFG(pd_reset,0,32,0);

   while(1){
      do_stuff();
   }
}
