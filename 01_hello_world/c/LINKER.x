ENTRY(_start)
MEMORY{
   rom (rx)  : ORIGIN = 0x00000000 LENGTH = 16K
   ram (!rx) : ORIGIN = 0x20000000 LENGTH = 2K
}
SECTIONS
{
   .init 0x0 : {
      *(.init)
   }>rom
   .text : {
      *(.text)
   }>rom
   .srodata : {
      *(.srodata)
   }>rom
   .data : {
      *(.data)
   } >ram
   .bss : {
      *(.bss)
   } >ram
}

