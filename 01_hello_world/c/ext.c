#include "common.h"

void do_stuff(){
   // test if setting PB9 is possible
   volatile unsigned int i = 0;
   SET_CFG(pd_set_reset,1,1,3);
   while(i < 1*500*1000) i++;
   SET_CFG(pd_set_reset,1,1,19);
   while(i < 2*500*1000) i++;
}
