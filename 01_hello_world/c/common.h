#ifndef HELLO_WORLD_COMMON_H
#define HELLO_WORLD_COMMON_H

#define SET_CFG(reg,val,val_width,bit_at) *reg = ((*reg)&(~((0xFFFFFFFF>>(32-val_width))<<(bit_at))))|((val)<<(bit_at))

typedef enum{
   CNF_GENERAL_PUSH_PULL,
   CNF_GENERAL_OPEN_DRAIN,
   CNF_ALTERNATE_FUNCTION_PUSH_PULL,
   CNF_ALTERNATE_FUNCTION_OPEN_DRAIN,
} Cnf;
typedef enum{
   PINMODE_INPUT,
   PINMODE_OUTPUT_SLOW,
   PINMODE_OUTPUT_MODERATE,
   PINMODE_OUTPUT_FAST
} PinMode;

#define pd_cfg_low      ((unsigned int*)0x40011400)
#define pd_cfg_high     ((unsigned int*)0x40011404)
#define pd_input        ((unsigned int*)0x40011408) 
#define pd_output       ((unsigned int*)0x4001140C) 
#define pd_set_reset    ((unsigned int*)0x40011410) 
#define pd_reset        ((unsigned int*)0x40011414) 
#define pd_cfg_lock     ((unsigned int*)0x40011418) 

#define rcc_apb2pcenr   ((unsigned int*)0x40021218)
#define rcc_rstsckr     ((unsigned int*)0x40021024)

#endif//HELLO_WORLD_COMMON_H
