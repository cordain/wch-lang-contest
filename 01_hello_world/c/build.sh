#!/bin/bash
WCH_TOOLCHAIN=$HOME/.dev/tools/MRS_Toolchain_Linux_x64_V1.91

GCC_TOOLCHAIN_BIN=$WCH_TOOLCHAIN/RISC-V_Embedded_GCC/bin
TARGET_TRIPLE="riscv-none-embed"
GCC=$GCC_TOOLCHAIN_BIN/$TARGET_TRIPLE-gcc
OBJCOPY=$GCC_TOOLCHAIN_BIN/$TARGET_TRIPLE-objcopy
LD=$GCC_TOOLCHAIN_BIN/$TARGET_TRIPLE-ld
BUILD_DIR=./build

OPENOCD=$WCH_TOOLCHAIN/OpenOCD/bin/openocd
GDB=$GCC_TOOLCHAIN_BIN/$TARGET_TRIPLE-gdb

ISA_FLAGS="-march=rv32ec -mabi=ilp32e"
CFLAGS="-O3 -c"

rm -r $BUILD_DIR
mkdir $BUILD_DIR

$GCC $ISA_FLAGS $CFLAGS c.c -o $BUILD_DIR/c.o
$GCC $ISA_FLAGS $CFLAGS ext.c -o $BUILD_DIR/ext.o
$GCC $ISA_FLAGS $CFLAGS start.s -o $BUILD_DIR/start.o
$LD -T LINKER.x -o $BUILD_DIR/elf $BUILD_DIR/c.o $BUILD_DIR/ext.o $BUILD_DIR/start.o
$OBJCOPY -O binary $BUILD_DIR/elf $BUILD_DIR/build.raw
$OBJCOPY -O srec --srec-len=32 $BUILD_DIR/elf $BUILD_DIR/build.srec

## FLASH RUN
$OPENOCD -f openocd.cfg  -c init -c halt -c "program build/elf" -c reset -c exit

## DEBUG RUN
#$OPENOCD &
#$GDB -x run.gdb
#killall openocd
